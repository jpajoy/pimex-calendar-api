const { Router } = require('express')
const admin = require('firebase-admin')
const Google = require('../utils/google')
const db = admin.firestore()
const router = Router()

router.post('/', async (req, res) => {
  const body = req.body
  try {
    const event = await (await db.collection('calendars').add(body)).get()
    const data = event.data()
    const result = {
      id: event.id,
      ...data
    }
    return res.status(201).json(result)
  } catch (e) {
    console.log(e)
    return res.status(500).json(e)
  }
})

router.get('/board/:boardId', async (req, res) => {
  const boardId = req.params.boardId
  try {
    const events = (
      await db
        .collection('calendars')
        .where('board.id', '==', boardId)
        .get()
    ).docs.map(e => {
      const data = e.data()
      return {
        id: e.id,
        ...data
      }
    })
    return res.status(200).json(events)
  } catch (e) {
    console.log(e)
    return res.status(500).json(e)
  }
})

router.get('/:id', async (req, res) => {
  const calendarId = req.params.id
  try {
    const calendar = await db
      .collection('calendars')
      .doc(calendarId)
      .get()
    if (!calendar.exists) {
      return res.status(404).json({})
    }
    return res.status(200).json({ id: calendar.id, ...calendar.data() })
  } catch (e) {
    console.log(e)
    return res.status(500).json(e)
  }
})

router.put('/:id', async (req, res) => {
  const calendarId = req.params.id
  const body = req.body
  try {
    const event = await db
      .collection('calendars')
      .doc(calendarId)
      .update(body)
    return res.status(200).json(event)
  } catch (e) {
    console.log(e)
    return res.status(500).json(e)
  }
})

router.delete('/:calendarId', async (req, res) => {
  const calendarId = req.params.calendarId
  try {
    await db
      .collection('events')
      .where('calendarId', '==', calendarId)
      .get()
      .then(async query => {
        await Promise.all(
          query.docs.map(async data => {
            await window.db
              .collection('events')
              .doc(data.id)
              .delete()
          })
        )
      })
    const result = await db
      .collection('calendars')
      .doc(calendarId)
      .delete()
    return res.status(200).json(result)
  } catch (e) {
    console.log(e)
    return res.status(500).json(e)
  }
})

router.get('/user/:userEmail', async (req, res) => {
  const userEmail = req.params.userEmail
  try {
    const emailRef = await db
      .collection('calendarUsers')
      .doc(Buffer.from(userEmail, 'utf8').toString('base64'))
      .get()
    if (!emailRef.exists) {
      return res.status(404).json({ exists: false })
    }
    return res.status(200).json({ exists: true })
  } catch (e) {
    return res.status(500).json({ exists: false })
  }
})

router.post('/user/:userEmail', async (req, res) => {
  const userEmail = req.params.userEmail
  const authCode = req.query.code
  try {
    const refreshToken = await Google.getRefreshToken(authCode)
    await db
      .collection('calendarUsers')
      .doc(Buffer.from(userEmail, 'utf8').toString('base64'))
      .set({
        token: refreshToken
      })
    return res.status(204).json({})
  } catch (e) {
    return res.status(500).json({})
  }
})

module.exports = router
