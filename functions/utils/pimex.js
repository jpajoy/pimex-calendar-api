const axios = require('axios')
const config = require('../config.json')
const baseUrl = config.pimexApi.url

class Pimex {
  static async getLead (leadId) {
    const { data } = await axios.get(`${baseUrl}/conversions/${leadId}`)
    return data.data
  }

  static async addLead (leadData) {
    const { data } = await axios.post(`${baseUrl}/conversions/`, leadData)
    return data
  }

  static async addLeadTask (leadId, taskData) {
    const { data } = await axios.post(
      `${baseUrl}/conversions/${leadId}/tasks`,
      taskData,
      {
        headers: {
          Authorization: config.pimexApi.token
        }
      }
    )
    return data
  }
}

module.exports = Pimex
