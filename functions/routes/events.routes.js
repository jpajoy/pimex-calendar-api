const { Router } = require('express')
const admin = require('firebase-admin')
const Google = require('../utils/google')
const Pimex = require('../utils/pimex')
const db = admin.firestore()
const router = Router()

router.get('/', async (req, res) => {
  const { field, value } = req.query
  try {
    const events = (
      await db
        .collection('meetings')
        .where(field, '==', value)
        .get()
    ).docs.map(e => {
      const data = e.data()
      return {
        id: e.id,
        ...data
      }
    })
    return res.status(200).json(events)
  } catch (e) {
    console.log(e)
    return res.status(500).json(e)
  }
})

router.get('/:meetingId', async (req, res) => {
  const { meetingId } = req.params
  try {
    const meeting = await db
      .collection('calendars')
      .doc(meetingId)
      .get()
    if (!meeting.exists) {
      return res.status(404).json({})
    }
    return res.status(200).json({ id: meeting.id, ...meeting.data() })
  } catch (e) {
    console.log(e)
    return res.status(500).json(e)
  }
})

router.patch('/state/:meetingId', async (req, res) => {
  const { meetingId } = req.params
  const { newState } = req.body
  try {
    const ref = db.collection('meetings').doc(meetingId)
    const result = await ref.update({ state: newState })
    return res.status(200).json(result)
  } catch (e) {
    console.log(e)
    return res.status(500).json(e)
  }
})

router.post('/', async (req, res) => {
  const { calendarInfo, selectedHour } = req.body
  try {
    const result = await Google.insertEvent(calendarInfo, selectedHour)
    return res.status(200).json(result)
  } catch (e) {
    console.log(e)
    return res.status(500).json(e)
  }
})

router.put('/:meetingId', async (req, res) => {
  const { meetingId } = req.params
  const meetingData = req.body
  try {
    const ref = db.collection('meetings').doc(meetingId)
    const result = await ref.update(meetingData)
    return res.status(200).json(result)
  } catch (e) {
    console.log(e)
    return res.status(500).json(e)
  }
})

router.post('/verify/:leadId', async (req, res) => {
  const leadId = req.params.leadId
  try {
    const leadData = await Pimex.getLead(leadId)
    const eventId = leadData.custom.eventId
    const eventRef = await db
      .collection('meetings')
      .doc(eventId)
      .get()
    const eventData = { id: eventRef.id, ...eventRef.data() }
    if (eventData.state === 'Agendado') {
      const ref = db.collection('meetings').doc(eventId)
      await ref.update({ state: 'Confirmado' })
      res.status(200).json({
        state: true,
        message: 'Successfully confirmed'
      })
    } else {
      res.status(400).json({
        state: false,
        message: 'Already confirmed or cancelled'
      })
    }
  } catch (e) {
    console.log(e)
    return res.status(500).json(e)
  }
})

router.post('/cancel/:leadId', async (req, res) => {
  const leadId = req.params.leadId
  try {
    const leadData = await Pimex.getLead(leadId)
    const eventId = leadData.custom.eventId
    const eventRef = await db
      .collection('meetings')
      .doc(eventId)
      .get()
    const eventData = { id: eventRef.id, ...eventRef.data() }
    if (eventData.state === 'Agendado') {
      const ref = db.collection('meetings').doc(eventId)
      await ref.update({ state: 'Cancelado' })
      res.status(200).json({
        state: true,
        message: 'Successfully cancelled'
      })
    } else {
      res.status(400).json({
        state: false,
        message: 'Already confirmed or cancelled'
      })
    }
  } catch (e) {
    console.log(e)
    return res.status(500).json(e)
  }
})

module.exports = router
