const functions = require('firebase-functions')
const express = require('express')
const admin = require('firebase-admin')
const serviceAccount = require('./keys/firebase-key.json')
const cors = require('cors')
const bodyParser = require('body-parser')

const app = express()

// Automatically allow cross-origin requests
app.use(cors({ origin: true }))

app.use(bodyParser.json())

app.use(express.static('public'))

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: 'pimex-calendar.appspot.com/'
})

app.use('/calendar', require('./routes/calendar.routes'))
app.use('/event', require('./routes/events.routes'))
app.use('/google', require('./routes/google.routes'))

exports.app = functions.https.onRequest(app)

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
